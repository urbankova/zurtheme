<!-- 
    template for subpages
 -->

 <!-- napoji header-subpage.php na page -->
 <!-- argument je to co je v nazvu header souboru za 'header-'; nazev header souboru
 musi zacinat 'header-' aby to slo includnout pomoci get_header. nesmi to byt ve slozce,
 abych to mohla dat do slozky, musela bych pouzit get_template_part, coz funguje skoro stejne -->
 <!-- to same lze udelat i pro footer -->
 <?php get_header('subpage');?>

<p>html: sidebar from page.php:</p>
 <?php if(is_active_sidebar('page-sidebar')) {
     dynamic_sidebar('page-sidebar');
 } ?>

<h1>html: page.php page</h1>
<h2> <?php the_title(); ?> </h2>

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();
        the_content();
    }
}
?>



<!-- napoji footer.php na page -->
 <?php get_footer();?>