<?php 
/*
Template Name: Contact
*/

// the comment above is what tells Wordpress that this is a page template
// this is how you create a template for each page separately
// the name does not need to start with 'template', here it's for organizational purposes
// you need to assign this page template to the corresponding page in wpcms
?>

<?php get_header();?>

<h1>html: template-contact.php page</h1>
<h2> <?php the_title(); ?> </h2>

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();
        the_content();
    }
}
?>



<!-- napoji footer.php na page -->
 <?php get_footer();?>