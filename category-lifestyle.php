<!-- 
    template for blog (list of articles)
    you can have more archive.php pages for different blogpost categories
 -->

 <!-- napoji header.php na blog -->
 <?php get_header();?>

<h1>html: category-lifestyle.php page</h1>
<h2> <?php single_cat_title(); // blog cathegory title ?> </h2>

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();?>
        
        <!-- title -->
        <h3><?php the_title(); ?> </h3>
        
        <!-- thumbnail -->
        <?php if(has_post_thumbnail()){ // view post's featured image if it exists?> 
            <img src="<?php the_post_thumbnail_url('small');?>">
        <?php }; ?> 

        <!-- perex -->
        <?php the_excerpt();?>

        <a href="<?php the_permalink(); // odkaz na blogpost?>">Read More</a> <?php
    }
}

// previous and next links for pagination. we need to se the number of post per page in wpcms (settings -> reading)
// previous_posts_link();
// next_posts_link();

// numbered pagination:
global $wp_query;
$big = 9999999999;
echo paginate_links(array (
    'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format'    => '?paged=%#%',
    'current'   => max(1, get_query_var('paged')),
    'total'     => $wp_query->max_num_pages
));
?>



<!-- napoji footer.php na blog -->
 <?php get_footer();?>