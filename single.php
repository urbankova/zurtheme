<!-- 
    template for blog detail

 -->

 <!-- napoji header.php na blog detail -->
 <?php get_header();?>

<h1>html: single.php page</h1>
<h2> <?php the_title(); // blogpost title ?> </h2>


<?php if(has_post_thumbnail()){ // view post's featured image if it exists?> 
    <img src="<?php the_post_thumbnail_url('small');?>">
<?php }; ?> 

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();
        the_content();

        // vypise username autora blogpostu (bere se z wpcms):
        the_author(); 
        
        // vypsani jmena a prijmeni autora blogpostu (bere se z wpcms):
        echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name');
        
        // vypise datum vytvoreni blogpostu; lze definovat format data podle php date:
        echo get_the_date('d/m/Y'); 

        // vypsani tagu daneho blogpostu; na tag jde kliknout a zobrazi vsechny blogposty s danym tagem:
        $tags = get_the_tags();
        foreach($tags as $tag) {
            // echo get_tag_link($tag->term_id); // vraci link
            ?>
            <a href="<?php echo get_tag_link($tag->term_id); ?>">
                <?php echo $tag->name;?>
            </a>
            <?php
        }

        // vypsani kategorii do kterych patri dany blogpost:
        $categories = get_the_category();
        foreach($categories as $cat) {
            ?>
            <a href="<?php echo get_category_link($cat->term_id); ?>">
                <?php echo $cat->name;?>
            </a>
            <?php
        }

        // we can create 'pages' in a single blogpost - the annoying bit where you have to click to view the next recipe or whatever in an article
        // v wpcms dam edit post, prepnu se do textoveho rezimu, a pridam '<!--nextpage-->' na mista kde chci page break
        wp_link_pages();

        // add comments
        comments_template();

    }
}
?>



<!-- napoji footer.php na blog detail -->
 <?php get_footer();?>