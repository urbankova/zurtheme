<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- this is a hook for wordpress and plugins so that they can add their scripts to the header -->
    <?php wp_head();?>


</head>
<!-- this allows wp to add its own classes on the body -->
<body <?php body_class() ?>>
<header>
    <?php 
        wp_nav_menu(
            array(
                'theme_location' => 'topMenu',
                'menu_class' => 'navigation' // adds .navigation to the menu's <ul> tag
            )
        );
    ?>

    <h1>html: this is the subpage header</h1>

</header>