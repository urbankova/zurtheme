<?php // file that overrides default wordpress search form ?>

<form action="/" method="get">
    <label for="search">Search</label>

    <!-- bude vyhledavat jenom v zadane kategorii blogu - cislo zjistim v wpcms podle odkazu kategorie v inspektoru; name musi byt 'cat' -->
    <input type="hidden" name="cat" value="4">
    
    <!-- name musi byt 's' - podle toho wordpress pozna ze to ma byt search -->
    <input type="text" name="s" id="search" value="<?php the_search_query();?>" required>
    <button type="submit">Search</button>
</form>