<!-- 
    template for blog detail

 -->

 <!-- napoji header.php na blog detail -->
 <?php get_header();?>

<h1>html: single-books.php page</h1>
<h2> <?php the_title(); // blogpost title ?> </h2>


<?php if(has_post_thumbnail()){ // view post's featured image if it exists?> 
    <img src="<?php the_post_thumbnail_url('small');?>">
<?php }; ?> 

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();
        the_content();

        ?>

        <!-- custom fields - the basic way without using a plugin (not recommended; use Advanced Custom Fields instead) -->
        <!-- <ul> -->
            <?php //if(get_post_meta($post->ID, 'Language', true)) { // jen pokud je tento custom field vyplnen ?>
                <!-- <li> -->
                    <!-- Language: <?php //echo get_post_meta($post->ID, 'Language', true); // id postu; nazev custom fieldu zadany v wpcms; true = string, false = array?> -->
                <!-- </li> -->
            <?php //}; ?>
            <!-- <li> -->
                <!-- Pages: <?php //echo get_post_meta($post->ID, 'Pages', true);?> -->
            <!-- </li> -->
            <!-- <li> -->
                <!-- Price: <?php //echo get_post_meta($post->ID, 'Price', true);?> -->
            <!-- </li> -->
        <!-- </ul> -->

        <!-- custom fileds using Advanced Custom Fields -->
        <!-- pokud pouzivam tenhle plugin tak nesmim mit naklikany zpusob zadavani custom fields -->
            <!-- <ul> -->
                <!-- <li> -->
                    <!-- Language: <?php the_field('language');?> -->
                <!-- </li> -->

                <!-- <li> -->
                    <!-- Pages: <?php the_field('pages');?> -->
                <!-- </li> -->
                <!-- <li> -->
                    <!-- Price: <?php the_field('price');?> -->
                <!-- </li> -->
            <!-- </ul> -->


        <!-- handcoded form -->
        <h2>Send an enquiry about <?php the_title() ?></h2>
        <form id="enquiry">
          <input type="text" name="fname" placeholder="First name" required>
          <input type="text" name="lname" placeholder="Last name" required>
          <input type="email" name="email" placeholder="Email address" required>
          <input type="tel" name="phone" placeholder="Phone" required>
          <textarea name="enquiry" cols="30" rows="10" placeholder="Your enquiry" required></textarea>
          <button type="submit">Send your enquiry</button>
        </form>

        <script>
          let formElm = document.getElementById("enquiry");
            formElm.addEventListener("submit", function(e) {
            console.log("submitted");
            e.preventDefault();

            let endpoint = " <?php echo admin_url('admin-ajax.php');?> "; // tady se v WP zpracovavaji formularove dotazy
            let formString = jQuery('#enquiry').serialize(); // pouzijeme jquery funkci serialize()
            console.log(formString);

            let formData = new FormData;
            formData.append('action', 'enquiry');
            formData.append('enquiry', formString);

            jQuery.ajax(endpoint, {
              type: 'POST',
              data: formData,
              processData: false, // turn off default ajax actions
              contentType: false, // what type of data are we sending
              
              // success and error callbacks for the ajax response
              success: function(res) {

              },

              error: function(err) {

              }

            });

          });

          // TODO: ajax; new FormData, content type, get vs post
        
        </script>


        <?php

        // vypise username autora blogpostu (bere se z wpcms):
        the_author(); 
        
        // vypsani jmena a prijmeni autora blogpostu (bere se z wpcms):
        echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name');
        
        // vypise datum vytvoreni blogpostu; lze definovat format data podle php date:
        echo get_the_date('d/m/Y'); 

        // vypsani tagu daneho blogpostu; na tag jde kliknout a zobrazi vsechny blogposty s danym tagem:
        $tags = get_the_tags();
        if($tags) {
            foreach($tags as $tag) {
                // echo get_tag_link($tag->term_id); // vraci link
                ?>
                <a href="<?php echo get_tag_link($tag->term_id); ?>">
                    <?php echo $tag->name;?>
                </a>
                <?php
            }
        }

        // vypsani kategorii do kterych patri dany blogpost:
        $categories = get_the_category();
        foreach($categories as $cat) {
            ?>
            <a href="<?php echo get_category_link($cat->term_id); ?>">
                <?php echo $cat->name;?>
            </a>
            <?php
        }

        // we can create 'pages' in a single blogpost - the annoying bit where you have to click to view the next recipe or whatever in an article
        // v wpcms dam edit post, prepnu se do textoveho rezimu, a pridam '<!--nextpage-->' na mista kde chci page break
        wp_link_pages();

        // add comments
        comments_template();

    }
}
?>



<!-- napoji footer.php na blog detail -->
 <?php get_footer();?>