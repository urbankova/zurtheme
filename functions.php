<?php 
    //  tady se includuji css a js, aby to bylo vsechno na jednom miste, plus aby pluginy mely pristup k temto includnutym stylum 
    //  (pokud bychom styly includovali v header.php, tak by se k nim pluginy nedostaly)
 
    //  aby se nacetlo css, musi se pouzit:
    //      - wp_register_style
    //      - wp_enqueue_style
    //      - add_action

    function loadCss() {
        // args:
        // style name
        // path to file
        // stylesheets this stylesheet depends on; here we pass in an empty array
        // stylesheet version number
        // css 'media' parameter for which we want to load the stylesheets, e.g. 'print', 'screen' etc.
        wp_register_style('mainStyles', get_template_directory_uri() . '/style.css', array(), false, 'all');
        wp_enqueue_style('mainStyles');
        wp_register_style('sassStyles', get_template_directory_uri() . '/styles.css', array(), false, 'all');
        wp_enqueue_style('sassStyles');

        // pokud bych includovala vice css souboru, je potreba je includovat ve spravnem poradi, aby se neprepisovaly styly

    }

    add_action('wp_enqueue_scripts', 'loadCss'); // pozor, tady musi byt enqueue scripts, protoze styles neni hook

    function loadJs() {
        // args:
        // script name
        // path to file
        // dependencies, here we pass an empty string
        // script version number
        // do we want to include the style before the closing body tag instead of in the <head>? true/false

        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.5.1.min.js', '', 1, true);
        wp_enqueue_script('jquery');
        
        wp_register_script('mainJs', get_template_directory_uri() . '/js/scripts.js', 'jquery', 1, true);
        wp_enqueue_script('mainJs');
        
        // wp_deregister_script('jquery'); // this is how you deregister jquery registered by wordpress, so that you can register your own
    }

    add_action('wp_enqueue_scripts', 'loadJs');


    
    // MENUS:
    // we need to:
    //  - add menu support
    //  - create menu in WP CMS
    //  - register nav menus = create menu locations
    //  - add created menu to created location 
    //  - add wp_nav_menu to header.php

    // allows us to create menus
    add_theme_support( 'menus' );

    // creating a menu location - there can be several places in the theme where a menu can be, here we define them:
    // only one menu can be assigned to a menu location in wpcms
    register_nav_menus(
        array(
            'topMenu' => __('Top menu', 'theme'),
            'footerMenu' => __('Footer menu', 'theme')
        )
    );

    add_theme_support('post-thumbnails'); // umozni vlozit do blogpostu obrazek
    add_image_size('small', 400, 300, true); // posledni argument - ma wp orezat obrazek presne na 400x400? (nezachova vysku/sirku); funguje pouze pro obrazky vetsi nez jsou specifikovane rozmery (zde 400x400)

    // add widgets support
    add_theme_support('widgets');

    // register sidebars to put widgets in
    function mySidebars() {
        register_sidebar(
            array(
                'name' => 'Page Sidebar',
                'id' => 'page-sidebar',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>'
            )
        );

        register_sidebar(
            array(
                'name' => 'Blog Sidebar',
                'id' => 'blog-sidebar',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>'
            )
        );
    }

    add_action('widgets_init', 'mySidebars');

    
    // pridani noveho 'druhu stranky/postu' do wpcms, ktery se pak objevi v menu a bude mozne v nem vytvaret clanky
    // tady vytvarim stranky pro knizky
    // v wpcms vytvorim post, a pokud se spravne nenacte, je potreba jit v wpcms do settings -> permalinks a kliknout save
    // nove vytvorene posty defaultne pouzivaji single.php template; pokud chci aby pouzily vlastni, musim vytvorit novou se
    // jmenem single-{nazev_post_typu}.php, tzn. zde single-books.php
    // stejnym zpusobem muzu vytvorit archive.php pro books
    function books_post_type() {
        register_post_type('books', array(
            'labels' => array( // pojmenovani v wpcms
                'name' => 'Books', // nazev kategorie, neco jako 'posts'
                'singular_name' => 'Book' // pouzije se treba v 'add new book'
            ),
            'hierarchical' => true, // true = acts like a page, false = acts like a post
            'menu_icon' => 'dashicons-book-alt', // vybrat wp ikonu z https://developer.wordpress.org/resource/dashicons/#image-crop
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'), // ktere wp funkcionality jsou podporovany timto novym post typem
            // 'rewrite' => array('slug' => 'booksies') // co bude v url za lomitkem
        ));
    }

    add_action('init', 'books_post_type');

    // zprovozneni kategorii pro post type books:
    // taxonomy je wp pojem pro kategorie post typu
    function books_taxonomy() {
        register_taxonomy('genres', array('books'), array( // 'genres' je nazev taxonomie, array('books') - k cemu vsemu se ma pridat tato taxonomie)
            'labels' => array( 
                'name' => 'Genres', 
                'singular_name' => 'Genre'
            ),
            'public' => true,
            'hierarchical' => true, // true = acts like a category, false = acts like a tag

        ));

    }

    add_action('init', 'books_taxonomy');


    // funkce pro zpracovani formu



?>