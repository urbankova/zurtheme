<!-- 
    template only for the front page
 -->

<!-- napoji header.php na homepage -->
 <?php get_header();?>

<h1>html: font-page.php page</h1>
<h2> <?php the_title(); ?> </h2>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
    <?php the_content() ?>
<?php endwhile; endif; ?>

<!-- arguments:
    - folder/cast jmena souboru pred pomlckou
    - cast jmena souboru za pomlckou
    -- pojmenovani je libovolne
-->
<?php get_template_part('sections/section', 'content') ?>


<!-- search: -->
<?php get_search_form(); ?>


<!-- napoji footer.php na homepage -->
 <?php get_footer();?>