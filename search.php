<?php // displays search results; v podstate je to archive.php ale s vysledky vyhledavani; vyhodi i blogposty i stranky; hleda v celem obsahu ?>


<!-- napoji header.php na blog -->
<?php get_header();?>

<h1>html: search.php page</h1>
<h1>Search results for <i><?php echo get_search_query()?></i>:</h1>
<h2> <?php single_cat_title(); // blog cathegory title ?> </h2>

<?php 
// tohle dela to same jako kod v front-page.php, jen je to jinak zapsane
if(have_posts()) {
    while(have_posts()) {
        the_post();?>
        
        <!-- title -->
        <h3><?php the_title(); ?> </h3>
        
        <!-- thumbnail -->
        <?php if(has_post_thumbnail()){ // view post's featured image if it exists?> 
            <img src="<?php the_post_thumbnail_url('small');?>">
        <?php }; ?> 

        <!-- perex -->
        <?php the_excerpt();?>

        <a href="<?php the_permalink(); // odkaz na blogpost?>">Read More</a> <?php
    }
} else {
    echo 'no search results';
}
?>

<!-- napoji footer.php na blog -->
 <?php get_footer();?>