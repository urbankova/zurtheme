<!-- 
    soubor s vecmi co jsou obvykle na konci html souboru, napr. ukonceni <body> tagu atd.
 -->



 <footer>
    <?php 
        wp_nav_menu(
            array(
                'theme_location' => 'footerMenu', // footerMenu je nazev menu location definovany v functions.php
                'menu_class' => 'navigation' // adds .navigation to the menu's <ul> tag
            )
        );
    ?>

</footer>

<!-- this is a hook for wordpress and plugins so that they can add their scripts to the footer -->
<?php wp_footer();?>

</body>
</html>